package com.robin.encuesta.dominio.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robin.encuesta.dominio.modelo.PreguntaEncuestaOpcion;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioPreguntaEncuestaOpcion;
import com.robin.encuesta.dominio.puerto.repositorio.RepositorioPreguntaEncuestaOpcion;

@Service
public class ServicioPreguntaEncuestaOpcion implements RepositorioPreguntaEncuestaOpcion{
	@Autowired
	IRepositorioPreguntaEncuestaOpcion myIRepositorioPregEnOp;

	@Override
	public PreguntaEncuestaOpcion crearPreguntaEncuestaOpcion(PreguntaEncuestaOpcion myPreEncOp) {
		return myIRepositorioPregEnOp.save(myPreEncOp);
	}

	@Override
	public PreguntaEncuestaOpcion getPreEncOpById(Long idPreEnOp) {
		Optional<PreguntaEncuestaOpcion> retValue = myIRepositorioPregEnOp.findById(idPreEnOp);
        if(retValue.isPresent()){
            return retValue.get();
        }
        return null;
	}

	@Override
	public List<PreguntaEncuestaOpcion> getAllPreEnOp() {
		List<PreguntaEncuestaOpcion> preguntasEncuestasOpciones = new ArrayList<>();
		Iterable<PreguntaEncuestaOpcion> lstPregEncuestasOpciones = myIRepositorioPregEnOp.findAll();
		lstPregEncuestasOpciones.forEach(preguntasEncuestasOpciones::add);
		return preguntasEncuestasOpciones;
	}
}
