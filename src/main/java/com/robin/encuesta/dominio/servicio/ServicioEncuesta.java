package com.robin.encuesta.dominio.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.robin.encuesta.dominio.excepcion.ExcepcionEncuestaNoEncontrada;
import com.robin.encuesta.dominio.excepcion.ExcepcionPreguntaNoEncontrada;
import com.robin.encuesta.dominio.excepcion.ExcepcionTipoEncuestaNoEncontrada;
import com.robin.encuesta.dominio.modelo.Encuesta;
import com.robin.encuesta.dominio.modelo.PreguntaEncuesta;
import com.robin.encuesta.dominio.modelo.PreguntaEncuestaOpcion;
import com.robin.encuesta.dominio.modelo.RetornoEncuesta;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioEncuesta;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioPreguntaEncuesta;
import com.robin.encuesta.dominio.puerto.repositorio.RepositorioEncuesta;

@Service
public class ServicioEncuesta implements RepositorioEncuesta {
	
	@Autowired
	private IRepositorioEncuesta myIReposotorioEncuesta;
	@Autowired
	private IRepositorioPreguntaEncuesta myIReposotorioPreEnc;
	@Autowired
    JdbcTemplate jdbc = new JdbcTemplate();
	@Autowired
	ServicioTipoPreguntaEncuesta myServicioTipPreEnc;
	@Autowired
	ServicioPreguntaEncuestaOpcion myServicioPreEncOp;
	@Autowired
	ServicioPreguntaEncuesta myServicioPreEnc;
	
	@Override
	public List<Encuesta> getEncuestas() {
		List<Encuesta> encuestas = new ArrayList<>();
		Iterable<Encuesta> lstEncuestas = myIReposotorioEncuesta.findAll();
		lstEncuestas.forEach(encuestas::add);
		return encuestas;
	}

	@Override
	public RetornoEncuesta getEncuestaId(Long idEncuesta) {
		Optional<Encuesta> retValue = myIReposotorioEncuesta.findById(idEncuesta);
		if(retValue.isPresent()) {
			if(myIReposotorioPreEnc.count() > 0) {
				String sql = "SELECT * FROM PREGUNTA_ENCUESTA WHERE ENC_ID =  ?";
				List<PreguntaEncuesta> retorno = new ArrayList<PreguntaEncuesta>();
		        List<Map<String, Object>> rows = jdbc.queryForList(sql, new Object[] {idEncuesta});
		        for(Map<String, Object> row:rows){
		        	Long id = (Long)row.get("pre_enc_id");
		        	String pregunta = (String) row.get("pre_enc_pregunta");
		        	Long idEnc = (Long)row.get("enc_id");
		        	Long idTipPreEnc = (Long)row.get("tip_pre_enc_id");
		        	PreguntaEncuesta myPregEncu = new PreguntaEncuesta(id, pregunta, getEncuestaById(idEnc), myServicioTipPreEnc.getTipPreEncuestById(idTipPreEnc));
		        	retorno.add(myPregEncu);
		        }
		        RetornoEncuesta myRetorno = new RetornoEncuesta(retValue.get(), retorno);
				return myRetorno;
			}else {
				RetornoEncuesta myRetorno = new RetornoEncuesta(retValue.get(), null);
				return myRetorno;
			}
		}
		return null;
	}

	@Override
	public Encuesta crearEncuesta(Encuesta myEncuesta) {
		return myIReposotorioEncuesta.save(myEncuesta);
	}
	
	public PreguntaEncuesta crearPreguntaEncuesta(PreguntaEncuesta myPreEncuesta) {
		if(myPreEncuesta != null) {
			if(myPreEncuesta.getMyEncuesta() != null) {
				if(myPreEncuesta.getMyTipoPreEnc() != null) {
					return myIReposotorioPreEnc.save(myPreEncuesta);
				}else {
					throw new ExcepcionTipoEncuestaNoEncontrada("El tipo pregunta encuesta no ha sido creada");
				}
			}else {
				throw new ExcepcionEncuestaNoEncontrada("La encuesta no ha sido creada");
			}
		}
		return null;
	}

	@Override
	public Encuesta getEncuestaById(Long idEncuesta) {
		Optional<Encuesta> retValue = myIReposotorioEncuesta.findById(idEncuesta);
        if(retValue.isPresent()){
            return retValue.get();
        }
        return null;
	}
	
	public PreguntaEncuestaOpcion crearPreguntaEncuestaOpcion(PreguntaEncuestaOpcion myPreEncOp) {
		if(myPreEncOp != null) {
			if(myServicioPreEnc.getPreguntaEncuestaById((myPreEncOp.getMyPreEnc().getPreEnc_id()))!= null )  {
				return myServicioPreEncOp.crearPreguntaEncuestaOpcion(myPreEncOp);
			}else {
				throw new ExcepcionPreguntaNoEncontrada("La pregunta no ha sido creada");
			}
		}
		return null;
	}

}
