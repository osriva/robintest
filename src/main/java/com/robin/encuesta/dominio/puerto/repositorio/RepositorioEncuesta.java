package com.robin.encuesta.dominio.puerto.repositorio;

import com.robin.encuesta.dominio.modelo.*;
import java.util.*;

public interface RepositorioEncuesta {
	
	List<Encuesta> getEncuestas();
	RetornoEncuesta getEncuestaId(Long idEncuesta);
	Encuesta crearEncuesta(Encuesta myEncuesta);
	Encuesta getEncuestaById(Long idEncuesta);
	
}
