package com.robin.encuesta.dominio.excepcion;

public class ExcepcionClienteNoEncontrado extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcepcionClienteNoEncontrado(String mensaje) {
		super(mensaje);
	}

}
