package com.robin.encuesta.dominio.puerto.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.robin.encuesta.dominio.modelo.TipoPreguntaEncuesta;

public interface IRepositorioTipoPreguntaEncuesta extends CrudRepository<TipoPreguntaEncuesta, Long>{

}
