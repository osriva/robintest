package com.robin.encuesta.dominio.puerto.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.robin.encuesta.dominio.modelo.PreguntaEncuesta;

public interface IRepositorioPreguntaEncuesta extends CrudRepository<PreguntaEncuesta, Long>{

}
