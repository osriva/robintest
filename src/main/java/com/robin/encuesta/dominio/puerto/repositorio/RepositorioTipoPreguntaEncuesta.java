package com.robin.encuesta.dominio.puerto.repositorio;

import java.util.List;

import com.robin.encuesta.dominio.modelo.TipoPreguntaEncuesta;

public interface RepositorioTipoPreguntaEncuesta {
	TipoPreguntaEncuesta getTipPreEncuestById(Long idTiPreEnc);
	TipoPreguntaEncuesta createTipPreEncuesta(TipoPreguntaEncuesta myTipPreEnc);
	List<TipoPreguntaEncuesta> getAllTipPregEncuesta();
}
