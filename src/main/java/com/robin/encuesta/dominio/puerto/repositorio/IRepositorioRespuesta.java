package com.robin.encuesta.dominio.puerto.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.robin.encuesta.dominio.modelo.Respuesta;

public interface IRepositorioRespuesta extends CrudRepository<Respuesta, Long>{

}
