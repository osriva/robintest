package com.robin.encuesta.dominio.puerto.repositorio;

import java.util.List;

import com.robin.encuesta.dominio.modelo.PreguntaEncuesta;

public interface RepositorioPreguntaEncuesta {
	PreguntaEncuesta crearPreguntaEncuesta(PreguntaEncuesta myPreEncu);
	List<PreguntaEncuesta> getPreguntasEncuestas();
	PreguntaEncuesta getPreguntaEncuestaById(Long idPreEnc);
}
