package com.robin.encuesta.dominio.excepcion;

public class ExcepcionTipoEncuestaNoEncontrada extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcepcionTipoEncuestaNoEncontrada(String mensaje) {
		super(mensaje);
	}

}
