package com.robin.encuesta.dominio.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="opcion")
public class Opcion {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long opc_id;
	private String opc_opcion;
	
	public Opcion() {
		super();
	}
	
	public Opcion(Long opc_id, String opc_opcion) {
		super();
		this.opc_id = opc_id;
		this.opc_opcion = opc_opcion;
	}

	public Long getOpc_id() {
		return opc_id;
	}

	public void setOpc_id(Long opc_id) {
		this.opc_id = opc_id;
	}

	public String getOpc_opcion() {
		return opc_opcion;
	}

	public void setOpc_opcion(String opc_opcion) {
		this.opc_opcion = opc_opcion;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Opcion{");
        sb.append("id=").append(opc_id);
        sb.append(", opcion='").append(opc_opcion);
        sb.append('}');
        return sb.toString();
	}
}
