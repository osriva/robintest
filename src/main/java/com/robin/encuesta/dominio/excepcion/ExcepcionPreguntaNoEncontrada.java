package com.robin.encuesta.dominio.excepcion;

public class ExcepcionPreguntaNoEncontrada extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcepcionPreguntaNoEncontrada(String mensaje) {
		super(mensaje);
	}

}
