package com.robin.encuesta.dominio.modelo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="cliente")
public class Cliente {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long cli_id;
	private String cli_nombre;
	private String cli_apellido;
	private String cli_direccion;
	private String cli_telefono;
	private String cli_correo;
	
	public Cliente() {
		super();
	}
	
	public Cliente(Long cli_id, String cli_nombre, String cli_apellido, String cli_direccion, String cli_telefono, String cli_correo) {
		super();
		this.cli_id = cli_id;
		this.cli_nombre = cli_nombre;
		this.cli_apellido = cli_apellido;
		this.cli_direccion = cli_direccion;
		this.cli_telefono = cli_telefono;
		this.cli_correo = cli_correo;
	}

	public Long getCli_id() {
		return cli_id;
	}

	public void setCli_id(Long cli_id) {
		this.cli_id = cli_id;
	}

	public String getCli_nombre() {
		return cli_nombre;
	}

	public void setCli_nombre(String cli_nombre) {
		this.cli_nombre = cli_nombre;
	}

	public String getCli_apellido() {
		return cli_apellido;
	}

	public void setCli_apellido(String cli_apellido) {
		this.cli_apellido = cli_apellido;
	}

	public String getCli_direccion() {
		return cli_direccion;
	}

	public void setCli_direccion(String cli_direccion) {
		this.cli_direccion = cli_direccion;
	}

	public String getCli_telefono() {
		return cli_telefono;
	}

	public void setCli_telefono(String cli_telefono) {
		this.cli_telefono = cli_telefono;
	}

	public String getCli_correo() {
		return cli_correo;
	}

	public void setCli_correo(String cli_correo) {
		this.cli_correo = cli_correo;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Cliente{");
        sb.append("id=").append(cli_id);
        sb.append(", nombre='").append(cli_nombre).append('\'');
        sb.append(", apellido='").append(cli_apellido).append('\'');
        sb.append(", direccion='").append(cli_direccion).append('\'');
        sb.append(", telefono='").append(cli_telefono).append('\'');
        sb.append(", correo='").append(cli_correo);
        sb.append('}');
        return sb.toString();
	}
	
}
