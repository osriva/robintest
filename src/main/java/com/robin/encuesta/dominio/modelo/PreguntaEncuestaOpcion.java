package com.robin.encuesta.dominio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="pregunta_encuesta_opcion")
public class PreguntaEncuestaOpcion {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long preEncOpc_id;
	//@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private PreguntaEncuesta myPreEnc;
	//@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	Opcion myOpcion;
	
	public PreguntaEncuestaOpcion() {
		super();
	}
	
	//public PreguntaEncuestaOpcion(Long preEncOpc_id, PreguntaEncuesta myPreEnc, List<Opcion> lstOpcion) {
	public PreguntaEncuestaOpcion(Long preEncOpc_id, PreguntaEncuesta myPreEnc, Opcion myOpcion) {
		super();
		this.preEncOpc_id = preEncOpc_id;
		this.myPreEnc = myPreEnc;
		//this.listOpcion = lstOpcion;
		this.myOpcion = myOpcion;
	}

	public Long getPreEncOpc_id() {
		return preEncOpc_id;
	}

	public void setPreEncOpc_id(Long preEncOpc_id) {
		this.preEncOpc_id = preEncOpc_id;
	}

	public PreguntaEncuesta getMyPreEnc() {
		return myPreEnc;
	}

	public void setMyPreEnc(PreguntaEncuesta myPreEnc) {
		this.myPreEnc = myPreEnc;
	}

	public Opcion getMyOpcion() {
		return myOpcion;
	}

	public void setMyOpcion(Opcion myOpcion) {
		this.myOpcion = myOpcion;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PreguntaEncuestaOpcion{");
        sb.append("id=").append(preEncOpc_id);
        sb.append(", preguntaEncuesta='").append(myPreEnc).append('\'');
        sb.append(", opcion='").append(myOpcion);
        sb.append('}');
        return sb.toString();
	}

}
