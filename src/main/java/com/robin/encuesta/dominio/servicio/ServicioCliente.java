package com.robin.encuesta.dominio.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robin.encuesta.dominio.modelo.Cliente;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioCliente;
import com.robin.encuesta.dominio.puerto.repositorio.RepositorioCliente;

@Service
public class ServicioCliente implements RepositorioCliente{
	
	@Autowired
	IRepositorioCliente myIRepositorioCliente;
	
	@Override
	public Cliente getClienteById(Long idCliente) {
		Optional<Cliente> retValue = myIRepositorioCliente.findById(idCliente);
        if(retValue.isPresent()){
            return retValue.get();
        }
        return null;
	}

	@Override
	public List<Cliente> getClientes() {
		List<Cliente> clientes = new ArrayList<>();
		Iterable<Cliente> lstRespuestas = myIRepositorioCliente.findAll();
		lstRespuestas.forEach(clientes::add);
		return clientes;
	}

	@Override
	public Cliente createCliente(Cliente myCliente) {
		return myIRepositorioCliente.save(myCliente);
	}

}
