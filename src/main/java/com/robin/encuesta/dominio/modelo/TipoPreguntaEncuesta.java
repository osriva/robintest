package com.robin.encuesta.dominio.modelo;

import javax.persistence.*;


@Entity
@Table(name="tipo_pregunta_encuesta")
public class TipoPreguntaEncuesta {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long tipPreEnc_id;
	private String tipPreEnc_nombre;
	
	public TipoPreguntaEncuesta() {
		super();
	}
	
	public TipoPreguntaEncuesta(Long tipPreEnc_id, String tipoPreEnc_nombre) {
		super();
		this.tipPreEnc_id = tipPreEnc_id;
		this.tipPreEnc_nombre = tipoPreEnc_nombre;
	}

	public Long getTipPreEnc_id() {
		return tipPreEnc_id;
	}

	public void setTipPreEnc_id(Long tipPreEnc_id) {
		this.tipPreEnc_id = tipPreEnc_id;
	}

	public String getTipPreEnc_nombre() {
		return tipPreEnc_nombre;
	}

	public void setTipPreEnc_nombre(String tipPreEnc_nombre) {
		this.tipPreEnc_nombre = tipPreEnc_nombre;
	}
		
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("TipoPreguntaEncuesta{");
        sb.append("id=").append(tipPreEnc_id);
        sb.append(", nombre='").append(tipPreEnc_nombre);
        sb.append('}');
        return sb.toString();
	}
	
}
