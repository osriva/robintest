package com.robin.encuesta.dominio.puerto.repositorio;

import java.util.List;

import com.robin.encuesta.dominio.modelo.PreguntaEncuestaOpcion;

public interface RepositorioPreguntaEncuestaOpcion {
	PreguntaEncuestaOpcion crearPreguntaEncuestaOpcion(PreguntaEncuestaOpcion myPreEncOp);
	PreguntaEncuestaOpcion getPreEncOpById(Long idPreEnOp);
	List<PreguntaEncuestaOpcion> getAllPreEnOp();
}
