package com.robin.encuesta.dominio.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robin.encuesta.dominio.modelo.PreguntaEncuesta;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioPreguntaEncuesta;
import com.robin.encuesta.dominio.puerto.repositorio.RepositorioPreguntaEncuesta;

@Service
public class ServicioPreguntaEncuesta implements RepositorioPreguntaEncuesta{
	@Autowired
	IRepositorioPreguntaEncuesta myIReposotorioPreguntaEncuesta;

	@Override
	public PreguntaEncuesta crearPreguntaEncuesta(PreguntaEncuesta myPreEncu) {
		return myIReposotorioPreguntaEncuesta.save(myPreEncu);
	}

	@Override
	public List<PreguntaEncuesta> getPreguntasEncuestas() {
		List<PreguntaEncuesta> preguntasEncuestas = new ArrayList<>();
		Iterable<PreguntaEncuesta> lstPregEncuestas = myIReposotorioPreguntaEncuesta.findAll();
		lstPregEncuestas.forEach(preguntasEncuestas::add);
		return preguntasEncuestas;
	}

	@Override
	public PreguntaEncuesta getPreguntaEncuestaById(Long idPreEnc) {
		Optional<PreguntaEncuesta> retValue = myIReposotorioPreguntaEncuesta.findById(idPreEnc);
        if(retValue.isPresent()){
            return retValue.get();
        }
        return null;
	}
	
	
}
