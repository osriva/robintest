package com.robin.encuesta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobinTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(RobinTestApplication.class, args);
	}

}
