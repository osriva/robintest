package com.robin.encuesta;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.robin.encuesta.dominio.modelo.Cliente;
import com.robin.encuesta.dominio.modelo.Encuesta;
import com.robin.encuesta.dominio.modelo.Opcion;
import com.robin.encuesta.dominio.modelo.PreguntaEncuesta;
import com.robin.encuesta.dominio.modelo.PreguntaEncuestaOpcion;
import com.robin.encuesta.dominio.modelo.Respuesta;
import com.robin.encuesta.dominio.modelo.TipoPreguntaEncuesta;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@Sql(scripts = "/datos.sql") // to init sample DB data
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
@AutoConfigureMockMvc
class EncuestaTest {
	
	@Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;
	
	@Test
	public void listarEncuestaConSusRespectivasPreguntas() throws Exception{
		Encuesta myEncuesta = new Encuesta((long) 1,"Encuesta de prueba", "Esta es una encuesta de prueba");
		MvcResult resultadoEncuestaCreada = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myEncuesta)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.enc_id").exists())
                .andExpect(jsonPath("$.enc_nombre", is("Encuesta de prueba")))
                .andReturn();
		
		ResultadoEncuestaTest resultadoEncuesta = objectMapper.readValue(resultadoEncuestaCreada.getResponse().getContentAsString(), ResultadoEncuestaTest.class);
		TipoPreguntaEncuesta myTipoPreEnc = new TipoPreguntaEncuesta((long)1, "Abierta");
		PreguntaEncuesta myPreEnc = new PreguntaEncuesta((long)1, "Ingrese su nombre", myEncuesta, myTipoPreEnc);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/pregunta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myPreEnc)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.preEnc_id").exists())
                .andReturn().getResponse().getContentAsString();
		
		myPreEnc = new PreguntaEncuesta((long)2, "Ingrese su Apellido", myEncuesta, myTipoPreEnc);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/pregunta")
		                .contentType(MediaType.APPLICATION_JSON)
		                .content(objectMapper.writeValueAsString(myPreEnc)))
		        .andExpect(status().isCreated())
		        .andExpect(jsonPath("$.preEnc_id").exists())
		        .andReturn().getResponse().getContentAsString();
				
		myTipoPreEnc = new TipoPreguntaEncuesta((long)2, "Multiple");
		myPreEnc = new PreguntaEncuesta((long)3, "Cual es su sexo", myEncuesta, myTipoPreEnc);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/pregunta")
		                .contentType(MediaType.APPLICATION_JSON)
		                .content(objectMapper.writeValueAsString(myPreEnc)))
		        .andExpect(status().isCreated())
		        .andExpect(jsonPath("$.preEnc_id").exists())
		        .andReturn().getResponse().getContentAsString();
				
		mvc.perform(MockMvcRequestBuilders
				.get("/encuesta/" + resultadoEncuesta.getEnc_id())
				.accept(MediaType.APPLICATION_JSON))
		        .andDo(print())
		        .andExpect(status().isOk())
		        .andExpect(jsonPath("$.myEncuesta").exists())
		        .andExpect(jsonPath("$.listPreguntas").isArray());
	}
	
	
	@Test
	public void listarEncuestaSinPreguntasCreadas() throws Exception{
		Encuesta myEncuesta = new Encuesta((long) 1,"Encuesta de prueba", "Esta es una encuesta de prueba");
		MvcResult resultadoEncuestaCreada = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myEncuesta)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.enc_id").exists())
                .andExpect(jsonPath("$.enc_nombre", is("Encuesta de prueba")))
                .andReturn();
		
		ResultadoEncuestaTest resultadoEncuesta = objectMapper.readValue(resultadoEncuestaCreada.getResponse().getContentAsString(), ResultadoEncuestaTest.class);
				
		mvc.perform(MockMvcRequestBuilders
				.get("/encuesta/" + resultadoEncuesta.getEnc_id())
				.accept(MediaType.APPLICATION_JSON))
		        .andDo(print())
		        .andExpect(status().isOk())
		        .andExpect(jsonPath("$.myEncuesta").exists())
		        .andExpect(jsonPath("$.listPreguntas").isEmpty());
	}
	
	@Test
	public void listarEncuestaSinEncuestaExistente() throws Exception{
						
		mvc.perform(MockMvcRequestBuilders
				.get("/encuesta/100")
				.accept(MediaType.APPLICATION_JSON))
		        .andDo(print())
		        .andExpect(status().isNoContent());
	}
	
	@Test
	public void registrarEncuesta() throws Exception{
		Encuesta myEncuesta = new Encuesta((long) 1,"Encuesta de prueba", "Esta es una encuesta de prueba");
		MvcResult resultadoEncuestaCreada = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myEncuesta)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.enc_id").exists())
                .andExpect(jsonPath("$.enc_nombre", is("Encuesta de prueba")))
                .andReturn();
		
		ResultadoEncuestaTest resultadoEncuesta = objectMapper.readValue(resultadoEncuestaCreada.getResponse().getContentAsString(), ResultadoEncuestaTest.class);
		TipoPreguntaEncuesta myTipoPreEnc = new TipoPreguntaEncuesta((long)1, "Abierta");
		myEncuesta = new Encuesta(resultadoEncuesta.getEnc_id(), resultadoEncuesta.getEnc_nombre(), resultadoEncuesta.getEnc_descripcion());
		PreguntaEncuesta myPreEnc = new PreguntaEncuesta((long)1, "Ingrese su nombre", myEncuesta, myTipoPreEnc);
		MvcResult resultadoPreEncCreadaOne = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/pregunta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myPreEnc)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.preEnc_id").exists())
                .andReturn();
		
		ResultadoPreguntaEncuestaTest resultadoPreEncOne = objectMapper.readValue(resultadoPreEncCreadaOne.getResponse().getContentAsString(), ResultadoPreguntaEncuestaTest.class);
		myPreEnc = new PreguntaEncuesta((long)2, "Ingrese su Apellido", myEncuesta, myTipoPreEnc);
		MvcResult resultadoPreEncCreadaTwo = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/pregunta")
		                .contentType(MediaType.APPLICATION_JSON)
		                .content(objectMapper.writeValueAsString(myPreEnc)))
		        .andExpect(status().isCreated())
		        .andExpect(jsonPath("$.preEnc_id").exists())
		        .andReturn();
		ResultadoPreguntaEncuestaTest resultadoPreEncTwo = objectMapper.readValue(resultadoPreEncCreadaTwo.getResponse().getContentAsString(), ResultadoPreguntaEncuestaTest.class);
		
		myTipoPreEnc = new TipoPreguntaEncuesta((long)2, "Multiple");
		myPreEnc = new PreguntaEncuesta((long)3, "Cual es su sexo", myEncuesta, myTipoPreEnc);
		MvcResult resultadoPreEncCreadaThree = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/pregunta")
		                .contentType(MediaType.APPLICATION_JSON)
		                .content(objectMapper.writeValueAsString(myPreEnc)))
		        .andExpect(status().isCreated())
		        .andExpect(jsonPath("$.preEnc_id").exists())
		        .andReturn();
		
		ResultadoPreguntaEncuestaTest resultadoPreEncThree = objectMapper.readValue(resultadoPreEncCreadaThree.getResponse().getContentAsString(), ResultadoPreguntaEncuestaTest.class);
		
		Opcion myOpcion = new Opcion((long)1, "Masculino");
		myPreEnc = new PreguntaEncuesta(resultadoPreEncThree.getPreEnc_id(), resultadoPreEncThree.getPreEnc_pregunta(), resultadoPreEncThree.getMyEncuesta(), resultadoPreEncThree.getMyTipoPreEnc());
		PreguntaEncuestaOpcion myPreEncOp = new PreguntaEncuestaOpcion((long) 1, myPreEnc, myOpcion);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/preguntaopcion")
		                .contentType(MediaType.APPLICATION_JSON)
		                .content(objectMapper.writeValueAsString(myPreEncOp)))
		        .andExpect(status().isCreated())
		        .andExpect(jsonPath("$.preEncOpc_id").exists())
		        .andReturn();
		
		myOpcion = new Opcion((long)2, "Femenino");
		myPreEncOp = new PreguntaEncuestaOpcion((long) 2, myPreEnc, myOpcion);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/preguntaopcion")
		                .contentType(MediaType.APPLICATION_JSON)
		                .content(objectMapper.writeValueAsString(myPreEncOp)))
		        .andExpect(status().isCreated())
		        .andExpect(jsonPath("$.preEncOpc_id").exists())
		        .andReturn();
		
		
		Cliente myCliente = new Cliente((long)1, "Pepito", "Perez", "Calle Falsa 123", "555111133", "pepito@micorreo.com");
		
		Respuesta myRespuesta = new Respuesta((long)1, "Pepito", new PreguntaEncuesta(resultadoPreEncOne.getPreEnc_id(), resultadoPreEncOne.getPreEnc_pregunta(), resultadoPreEncOne.getMyEncuesta(), resultadoPreEncOne.getMyTipoPreEnc()), null, myCliente);
		
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/registrar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myRespuesta)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.res_id").exists())
                .andReturn().getResponse().getContentAsString();
		
		myRespuesta = new Respuesta((long)2, "Perez", new PreguntaEncuesta(resultadoPreEncTwo.getPreEnc_id(), resultadoPreEncTwo.getPreEnc_pregunta(), resultadoPreEncTwo.getMyEncuesta(), resultadoPreEncTwo.getMyTipoPreEnc()), null, myCliente);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/registrar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myRespuesta)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.res_id").exists())
                .andReturn().getResponse().getContentAsString();
		
		
		/*myPreEnc = new PreguntaEncuesta(resultadoPreEncThree.getPreEnc_id(), resultadoPreEncThree.getPreEnc_pregunta(), resultadoPreEncThree.getMyEncuesta(), resultadoPreEncThree.getMyTipoPreEnc());
		
		Respuesta myRespuesta = new Respuesta((long)3, null, myPreEnc, myPreEncOp, myCliente);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/registrar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myRespuesta)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.res_id").exists())
                .andReturn().getResponse().getContentAsString();*/
	}
	
	@Test
	public void registrarEncuestaSinPreguntaExistente() throws Exception{
		Respuesta myRespuesta = new Respuesta((long)3, null, null, null, null);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/registrar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myRespuesta)))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
	}
	
	
	@Test
	public void registrarEncuestaSinRespuesta() throws Exception{
		Encuesta myEncuesta = new Encuesta((long) 1,"Encuesta de prueba", "Esta es una encuesta de prueba");
		MvcResult resultadoEncuestaCreada = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myEncuesta)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.enc_id").exists())
                .andExpect(jsonPath("$.enc_nombre", is("Encuesta de prueba")))
                .andReturn();
		
		ResultadoEncuestaTest resultadoEncuesta = objectMapper.readValue(resultadoEncuestaCreada.getResponse().getContentAsString(), ResultadoEncuestaTest.class);
		TipoPreguntaEncuesta myTipoPreEnc = new TipoPreguntaEncuesta((long)1, "Abierta");
		myEncuesta = new Encuesta(resultadoEncuesta.getEnc_id(), resultadoEncuesta.getEnc_nombre(), resultadoEncuesta.getEnc_descripcion());
		PreguntaEncuesta myPreEnc = new PreguntaEncuesta((long)1, "Ingrese su nombre", myEncuesta, myTipoPreEnc);
		MvcResult resultadoPreEncCreadaOne = mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/pregunta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myPreEnc)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.preEnc_id").exists())
                .andReturn();
		ResultadoPreguntaEncuestaTest resultadoPreEncOne = objectMapper.readValue(resultadoPreEncCreadaOne.getResponse().getContentAsString(), ResultadoPreguntaEncuestaTest.class);
		Cliente myCliente = new Cliente((long)1, "Pepito", "Perez", "Calle Falsa 123", "555111133", "pepito@micorreo.com");
		
		myPreEnc = new PreguntaEncuesta(resultadoPreEncOne.getPreEnc_id(), resultadoPreEncOne.getPreEnc_pregunta(), resultadoPreEncOne.getMyEncuesta(), resultadoPreEncOne.getMyTipoPreEnc());
		Respuesta myRespuesta = new Respuesta((long)1, null, myPreEnc, null, myCliente);
		mvc.perform(
                MockMvcRequestBuilders.post("/encuesta/registrar")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(myRespuesta)))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();
	}

}
