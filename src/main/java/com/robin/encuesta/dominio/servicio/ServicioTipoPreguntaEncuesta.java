package com.robin.encuesta.dominio.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robin.encuesta.dominio.modelo.TipoPreguntaEncuesta;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioTipoPreguntaEncuesta;
import com.robin.encuesta.dominio.puerto.repositorio.RepositorioTipoPreguntaEncuesta;

@Service
public class ServicioTipoPreguntaEncuesta implements RepositorioTipoPreguntaEncuesta{
	
	@Autowired
	IRepositorioTipoPreguntaEncuesta myIRepositorioTipPregEnc;
	
	@Override
	public TipoPreguntaEncuesta getTipPreEncuestById(Long idTiPreEnc) {
		Optional<TipoPreguntaEncuesta> retValue = myIRepositorioTipPregEnc.findById(idTiPreEnc);
        if(retValue.isPresent()){
            return retValue.get();
        }
        return null;
	}

	@Override
	public TipoPreguntaEncuesta createTipPreEncuesta(TipoPreguntaEncuesta myTipPreEnc) {
		return myIRepositorioTipPregEnc.save(myTipPreEnc);
	}

	@Override
	public List<TipoPreguntaEncuesta> getAllTipPregEncuesta() {
		List<TipoPreguntaEncuesta> tipPregEncuestas = new ArrayList<>();
		Iterable<TipoPreguntaEncuesta> lstTipPregEncuestas = myIRepositorioTipPregEnc.findAll();
		lstTipPregEncuestas.forEach(tipPregEncuestas::add);
		return tipPregEncuestas;
	}

}
