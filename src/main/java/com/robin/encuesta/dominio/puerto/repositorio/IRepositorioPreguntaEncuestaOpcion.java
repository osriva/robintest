package com.robin.encuesta.dominio.puerto.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.robin.encuesta.dominio.modelo.PreguntaEncuestaOpcion;

public interface IRepositorioPreguntaEncuestaOpcion extends CrudRepository<PreguntaEncuestaOpcion, Long>{

}
