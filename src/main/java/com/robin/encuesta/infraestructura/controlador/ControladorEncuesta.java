package com.robin.encuesta.infraestructura.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.robin.encuesta.dominio.excepcion.ExcepcionClienteNoEncontrado;
import com.robin.encuesta.dominio.excepcion.ExcepcionEncuestaNoEncontrada;
import com.robin.encuesta.dominio.excepcion.ExcepcionPreguntaNoEncontrada;
import com.robin.encuesta.dominio.excepcion.ExcepcionRespuestaNoEncontrada;
import com.robin.encuesta.dominio.excepcion.ExcepcionTipoEncuestaNoEncontrada;
import com.robin.encuesta.dominio.modelo.Encuesta;
import com.robin.encuesta.dominio.modelo.PreguntaEncuesta;
import com.robin.encuesta.dominio.modelo.PreguntaEncuestaOpcion;
import com.robin.encuesta.dominio.modelo.Respuesta;
import com.robin.encuesta.dominio.modelo.RetornoEncuesta;
import com.robin.encuesta.dominio.servicio.ServicioEncuesta;
import com.robin.encuesta.dominio.servicio.ServicioRespuesta;

@RestController
@RequestMapping("encuesta")
public class ControladorEncuesta {
	@Autowired
	ServicioEncuesta myServicioEncuesta;
	@Autowired
	ServicioRespuesta myServicioRespuesta;
	
	@GetMapping("/encuestas")
	public ResponseEntity<?> getEncuestas(){
		List<Encuesta> lstEncuesta = myServicioEncuesta.getEncuestas();
		if(lstEncuesta.size() > 0) {
			return new ResponseEntity<>(lstEncuesta, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(lstEncuesta, HttpStatus.NO_CONTENT);
		}
	}
	
	@GetMapping("/{idEncuesta}")
	public ResponseEntity<?> getEncuestaById(@PathVariable Long idEncuesta){
		RetornoEncuesta myRetornoEncuesta = myServicioEncuesta.getEncuestaId(idEncuesta);
		if(myRetornoEncuesta != null) {
			return new ResponseEntity<>(myRetornoEncuesta, HttpStatus.OK);
		}else {
			return new ResponseEntity<RetornoEncuesta>(myRetornoEncuesta,HttpStatus.NO_CONTENT);
		}
	}
	
	@PostMapping("")
	public ResponseEntity<?> createEncuesta(@Validated @RequestBody Encuesta encuesta){
		if(encuesta != null) {
			Encuesta myEncuesta = myServicioEncuesta.crearEncuesta(encuesta);
			return new ResponseEntity<>(myEncuesta, HttpStatus.CREATED);
		}
		return new ResponseEntity<>("Peticion erronea", HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("pregunta")
	public ResponseEntity<?> createPreEncuesta(@Validated @RequestBody PreguntaEncuesta preEncuesta){
		if(preEncuesta != null) {
			try {
				PreguntaEncuesta myPreEncuesta = myServicioEncuesta.crearPreguntaEncuesta(preEncuesta);
				return new ResponseEntity<>(myPreEncuesta, HttpStatus.CREATED);
			} catch (ExcepcionTipoEncuestaNoEncontrada|ExcepcionEncuestaNoEncontrada e) {
				return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
			
		}
		return new ResponseEntity<>("Peticion erronea", HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("registrar")
	public ResponseEntity<?> registerEncuesta(@Validated @RequestBody Respuesta respuesta){
		if(respuesta != null) {
			try {
				Respuesta myRespuesta = myServicioRespuesta.createRespuesta(respuesta);
				return new ResponseEntity<>(myRespuesta, HttpStatus.CREATED);
			} catch (ExcepcionClienteNoEncontrado|ExcepcionPreguntaNoEncontrada|ExcepcionRespuestaNoEncontrada ex) {
				return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
			}
			
		}
		return new ResponseEntity<>("Peticion errornea", HttpStatus.BAD_REQUEST);
	}
	
	@PostMapping("preguntaopcion")
	public ResponseEntity<?> createPreEncuestaOpcion(@Validated @RequestBody PreguntaEncuestaOpcion preEncuestaOpcion){
		if(preEncuestaOpcion != null) {
			try {
				PreguntaEncuestaOpcion myPreEncuestaOpcion = myServicioEncuesta.crearPreguntaEncuestaOpcion(preEncuestaOpcion);
				return new ResponseEntity<>(myPreEncuestaOpcion, HttpStatus.CREATED);
			} catch (ExcepcionTipoEncuestaNoEncontrada|ExcepcionEncuestaNoEncontrada e) {
				return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
			}
			
		}
		return new ResponseEntity<>("Peticion erronea", HttpStatus.BAD_REQUEST);
	}
}
