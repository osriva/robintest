package com.robin.encuesta.dominio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="pregunta_encuesta")
public class PreguntaEncuesta {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long preEnc_id;
	private String preEnc_pregunta;
	//@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
	@ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@JoinColumn(name = "enc_id")
	Encuesta myEncuesta;
	//@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
	@OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	@JoinColumn(name = "tipPreEnc_id")
	TipoPreguntaEncuesta myTipoPreEnc;
	
	public PreguntaEncuesta() {
		super();
	}
	
	public PreguntaEncuesta(Long preEnc_id, String preEnc_pregunta, Encuesta myEncuesta, TipoPreguntaEncuesta myTipoPreEnc) {
		super();
		this.preEnc_id = preEnc_id;
		this.preEnc_pregunta = preEnc_pregunta;
		this.myEncuesta = myEncuesta;
		this.myTipoPreEnc = myTipoPreEnc;
	}

	public Long getPreEnc_id() {
		return preEnc_id;
	}

	public void setPreEnc_id(Long preEnc_id) {
		this.preEnc_id = preEnc_id;
	}

	public String getPreEnc_pregunta() {
		return preEnc_pregunta;
	}

	public void setPreEnc_pregunta(String preEnc_pregunta) {
		this.preEnc_pregunta = preEnc_pregunta;
	}

	public Encuesta getMyEncuesta() {
		return myEncuesta;
	}

	public void setMyEncuesta(Encuesta myEncuesta) {
		this.myEncuesta = myEncuesta;
	}

	public TipoPreguntaEncuesta getMyTipoPreEnc() {
		return myTipoPreEnc;
	}

	public void setMyTipoPreEnc(TipoPreguntaEncuesta myTipoPreEnc) {
		this.myTipoPreEnc = myTipoPreEnc;
	}
	
	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("PreguntaEncuesta{");
        sb.append("id=").append(preEnc_id);
        sb.append(", pregunta='").append(preEnc_pregunta).append('\'');
        sb.append(", tipoPregunta='").append(myTipoPreEnc).append('\'');
        sb.append(", encuesta='").append(myEncuesta);
        sb.append('}');
        return sb.toString();
	}
				
}
