package com.robin.encuesta.dominio.puerto.repositorio;

import java.util.List;

import com.robin.encuesta.dominio.modelo.Cliente;

public interface RepositorioCliente {
	Cliente getClienteById(Long idCliente);
	List<Cliente> getClientes();
	Cliente createCliente(Cliente myCliente);
}
