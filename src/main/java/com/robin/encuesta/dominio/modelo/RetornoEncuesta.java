package com.robin.encuesta.dominio.modelo;

import java.util.List;

public class RetornoEncuesta {
	private Encuesta myEncuesta;
	private List<PreguntaEncuesta> listPreguntas;
	
	public RetornoEncuesta(Encuesta myEncuesta, List<PreguntaEncuesta> listPreguntas){
		super();
		this.myEncuesta = myEncuesta;
		this.listPreguntas = listPreguntas;
	}

	public Encuesta getMyEncuesta() {
		return myEncuesta;
	}

	public void setMyEncuesta(Encuesta myEncuesta) {
		this.myEncuesta = myEncuesta;
	}

	public List<PreguntaEncuesta> getListPreguntas() {
		return listPreguntas;
	}

	public void setListPreguntas(List<PreguntaEncuesta> listPreguntas) {
		this.listPreguntas = listPreguntas;
	}
			
}
