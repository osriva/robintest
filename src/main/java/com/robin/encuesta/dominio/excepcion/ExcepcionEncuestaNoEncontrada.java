package com.robin.encuesta.dominio.excepcion;

public class ExcepcionEncuestaNoEncontrada extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcepcionEncuestaNoEncontrada(String mensaje) {
		super(mensaje);
	}

}
