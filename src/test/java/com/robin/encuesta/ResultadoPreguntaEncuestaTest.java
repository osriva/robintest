package com.robin.encuesta;

import com.robin.encuesta.dominio.modelo.Encuesta;
import com.robin.encuesta.dominio.modelo.TipoPreguntaEncuesta;

public class ResultadoPreguntaEncuestaTest {
	private Long preEnc_id;
	private String preEnc_pregunta;
	Encuesta myEncuesta;
	TipoPreguntaEncuesta myTipoPreEnc;
	
	public ResultadoPreguntaEncuestaTest(Long preEnc_id, String preEnc_pregunta, Encuesta myEncuesta, TipoPreguntaEncuesta myTipoPreEnc) {
		super();
		this.preEnc_id = preEnc_id;
		this.preEnc_pregunta = preEnc_pregunta;
		this.myEncuesta = myEncuesta;
		this.myTipoPreEnc = myTipoPreEnc;
	}

	public Long getPreEnc_id() {
		return preEnc_id;
	}

	public void setPreEnc_id(Long preEnc_id) {
		this.preEnc_id = preEnc_id;
	}

	public String getPreEnc_pregunta() {
		return preEnc_pregunta;
	}

	public void setPreEnc_pregunta(String preEnc_pregunta) {
		this.preEnc_pregunta = preEnc_pregunta;
	}

	public Encuesta getMyEncuesta() {
		return myEncuesta;
	}

	public void setMyEncuesta(Encuesta myEncuesta) {
		this.myEncuesta = myEncuesta;
	}

	public TipoPreguntaEncuesta getMyTipoPreEnc() {
		return myTipoPreEnc;
	}

	public void setMyTipoPreEnc(TipoPreguntaEncuesta myTipoPreEnc) {
		this.myTipoPreEnc = myTipoPreEnc;
	}
	
	
}
