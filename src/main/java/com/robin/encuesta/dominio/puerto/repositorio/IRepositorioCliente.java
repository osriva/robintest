package com.robin.encuesta.dominio.puerto.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.robin.encuesta.dominio.modelo.Cliente;

public interface IRepositorioCliente extends CrudRepository<Cliente, Long>{

}
