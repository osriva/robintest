package com.robin.encuesta.dominio.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robin.encuesta.dominio.excepcion.ExcepcionClienteNoEncontrado;
import com.robin.encuesta.dominio.excepcion.ExcepcionPreguntaNoEncontrada;
import com.robin.encuesta.dominio.excepcion.ExcepcionRespuestaNoEncontrada;
import com.robin.encuesta.dominio.modelo.Respuesta;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioRespuesta;
import com.robin.encuesta.dominio.puerto.repositorio.RepositorioRespuesta;

@Service
public class ServicioRespuesta implements RepositorioRespuesta{

	@Autowired
	IRepositorioRespuesta myIRepositorioRespuesta;
	@Autowired
	ServicioCliente myServicioCliente;
	
	@Override
	public Respuesta createRespuesta(Respuesta myRespuesta) {
		if(myRespuesta != null) {
			if(myRespuesta.getMyCliente() != null && myServicioCliente.getClienteById(myRespuesta.getMyCliente().getCli_id()) != null) {
				if(myRespuesta.getMyPreEnc() != null) {
					if(myRespuesta.getRes_respuesta() != null || myRespuesta.getMyPreEncOp() != null) {
						return myIRepositorioRespuesta.save(myRespuesta);
					}else {
						throw new ExcepcionRespuestaNoEncontrada("La respuesta no ha sido enviada");
					}
				}else {
					throw new ExcepcionPreguntaNoEncontrada("La pregunta no ha sido creada");
				}
			}else {
				throw new ExcepcionClienteNoEncontrado("El cliente no ha sido creado");
			}
		}
		return null;
	}

	@Override
	public Respuesta getRespuestaById(Long idRespuesta) {
		Optional<Respuesta> retValue = myIRepositorioRespuesta.findById(idRespuesta);
        if(retValue.isPresent()){
            return retValue.get();
        }
        return null;
	}

	@Override
	public List<Respuesta> getAllRespuesta() {
		List<Respuesta> respuestas = new ArrayList<>();
		Iterable<Respuesta> lstRespuestas = myIRepositorioRespuesta.findAll();
		lstRespuestas.forEach(respuestas::add);
		return respuestas;
	}

}
