package com.robin.encuesta.dominio.excepcion;

public class ExcepcionRespuestaNoEncontrada extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ExcepcionRespuestaNoEncontrada(String mensaje) {
		super(mensaje);
	}

}
