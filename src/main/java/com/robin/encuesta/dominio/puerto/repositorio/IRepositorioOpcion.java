package com.robin.encuesta.dominio.puerto.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.robin.encuesta.dominio.modelo.Opcion;

public interface IRepositorioOpcion extends CrudRepository<Opcion, Long>{

}
