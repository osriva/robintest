package com.robin.encuesta.dominio.servicio;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.robin.encuesta.dominio.modelo.Opcion;
import com.robin.encuesta.dominio.puerto.repositorio.IRepositorioOpcion;
import com.robin.encuesta.dominio.puerto.repositorio.RepositorioOpcion;

@Service
public class ServicioOpcion implements RepositorioOpcion{
	@Autowired
	IRepositorioOpcion myIRepositorioOpcion;

	@Override
	public Opcion crearOpcion(Opcion myOpcion) {
		return myIRepositorioOpcion.save(myOpcion);
	}

	@Override
	public Opcion getOpcionById(Long idOpcion) {
		Optional<Opcion> retValue = myIRepositorioOpcion.findById(idOpcion);
        if(retValue.isPresent()){
            return retValue.get();
        }
        return null;
	}

	@Override
	public List<Opcion> getOpciones() {
		List<Opcion> opciones = new ArrayList<>();
		Iterable<Opcion> lstOpciones = myIRepositorioOpcion.findAll();
		lstOpciones.forEach(opciones::add);
		return opciones;
	}
	
	
}
