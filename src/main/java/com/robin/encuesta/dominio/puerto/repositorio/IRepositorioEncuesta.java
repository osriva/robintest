package com.robin.encuesta.dominio.puerto.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.robin.encuesta.dominio.modelo.Encuesta;

public interface IRepositorioEncuesta extends CrudRepository<Encuesta, Long> {

}
