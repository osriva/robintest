package com.robin.encuesta.dominio.puerto.repositorio;

import java.util.List;

import com.robin.encuesta.dominio.modelo.Respuesta;

public interface RepositorioRespuesta {
	Respuesta createRespuesta(Respuesta myRespuesta);
	Respuesta getRespuestaById(Long idRespuesta);
	List<Respuesta> getAllRespuesta();
}
