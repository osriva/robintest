package com.robin.encuesta.dominio.puerto.repositorio;

import java.util.List;

import com.robin.encuesta.dominio.modelo.Opcion;

public interface RepositorioOpcion {
	Opcion crearOpcion(Opcion myOpcion);
	Opcion getOpcionById(Long idOpcion);
	List<Opcion> getOpciones();
}
