package com.robin.encuesta.dominio.modelo;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="respuesta")
public class Respuesta {
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private Long res_id;
	private String res_respuesta;
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "preEnc_id")
	private PreguntaEncuesta myPreEnc;
	@ManyToOne(optional = true, fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "preEncOpc_id")
	private PreguntaEncuestaOpcion myPreEncOp;
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
	@JoinColumn(name = "cli_id")
	private Cliente myCliente;
	
	public Respuesta() {
		super();
	}
	
	public Respuesta(Long res_id, String res_respuesta, PreguntaEncuesta myPreEnc, PreguntaEncuestaOpcion myPreEncOp, Cliente myCliente){
		super();
		this.res_id = res_id;
		this.res_respuesta = res_respuesta;
		this.myPreEnc = myPreEnc;
		this.myPreEncOp = myPreEncOp;
		this.myCliente = myCliente;
	}

	public Long getRes_id() {
		return res_id;
	}

	public void setRes_id(Long res_id) {
		this.res_id = res_id;
	}

	public String getRes_respuesta() {
		return res_respuesta;
	}

	public void setRes_respuesta(String res_respuesta) {
		this.res_respuesta = res_respuesta;
	}

	public PreguntaEncuesta getMyPreEnc() {
		return myPreEnc;
	}

	public void setMyPreEnc(PreguntaEncuesta myPreEnc) {
		this.myPreEnc = myPreEnc;
	}

	public Cliente getMyCliente() {
		return myCliente;
	}

	public void setMyCliente(Cliente myCliente) {
		this.myCliente = myCliente;
	}
	
	
	public PreguntaEncuestaOpcion getMyPreEncOp() {
		return myPreEncOp;
	}

	public void setMyPreEncOp(PreguntaEncuestaOpcion myPreEncOp) {
		this.myPreEncOp = myPreEncOp;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Respuesta{");
        sb.append("id=").append(res_id);
        sb.append(", respuesta='").append(res_respuesta).append('\'');
        sb.append(", preguntaEncuesta='").append(myPreEnc).append('\'');
        sb.append(", preguntaEncuestaOpcion='").append(myPreEncOp).append('\'');
        sb.append(", cliente='").append(myCliente);
        sb.append('}');
        return sb.toString();
	}	

}
