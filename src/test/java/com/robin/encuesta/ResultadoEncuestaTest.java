package com.robin.encuesta;

public class ResultadoEncuestaTest {
	private Long enc_id;
	private String enc_nombre;
	private String enc_descripcion;
	
	public ResultadoEncuestaTest(Long enc_id, String enc_nombre, String enc_descripcion) {
		super();
		this.enc_id = enc_id;
		this.enc_nombre = enc_nombre;
		this.enc_descripcion = enc_descripcion;
	}

	public Long getEnc_id() {
		return enc_id;
	}

	public void setEnc_id(Long enc_id) {
		this.enc_id = enc_id;
	}

	public String getEnc_nombre() {
		return enc_nombre;
	}

	public void setEnc_nombre(String enc_nombre) {
		this.enc_nombre = enc_nombre;
	}

	public String getEnc_descripcion() {
		return enc_descripcion;
	}

	public void setEnc_descripcion(String enc_descripcion) {
		this.enc_descripcion = enc_descripcion;
	}

	
}
